import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import typescript from 'rollup-plugin-typescript2';
import { uglify } from 'rollup-plugin-uglify'

const externals = [
  '^react$',
  '^react-dom$',
]

const externalRegex = new RegExp(externals.join('|'));

const external = id => externalRegex.test(id)

export default {
  input: 'src/index.ts',
  output: {
    file: 'dist/index.js',
    format: 'esm',
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM',
    },
  },
  external,
  plugins: [
    typescript(),
    resolve({
      browser: true
    }),
    commonjs(),
    // uglify()
  ]
};
