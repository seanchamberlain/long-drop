import Registry, { Unsubscribe } from "./Registry";
import { DragSourceViewModelImpl } from "./DragSourceViewModel";

interface XYCoordinate {
  x: number;
  y: number;
}

interface DragOperationState {
  dragSourceId: string;
  dropTargetIds: string[];
  itemType: string;
  dragSourceViewModel: DragSourceViewModelImpl;
}

interface DragOperationOffsets {
  clientOffset: XYCoordinate;
  sourceClientOffset: XYCoordinate;
  initialSourceClientOffset: XYCoordinate;
  initialClientOffset: XYCoordinate;
  diff: XYCoordinate
}

interface DropResult { [key: string]: any };

interface DragOperation {
  state: DragOperationState;
  offsets: DragOperationOffsets;
  result: DropResult;
}

type SubscriptionListener = (model: Model) => void;
export type ShouldUpdateSubscription = (dirty: DirtyViews) => boolean;
export interface DirtyViews { dropTargets: string[], dragSources: string[] }

export interface Subscription {
  listener: SubscriptionListener;
  shouldUpdate?: ShouldUpdateSubscription;
}

export default class Model {
  registry: Registry;
  currentOperation: DragOperation | null = null;
  subscriptions: Subscription[] = [];
  dirtyViews: DirtyViews | null = null;

  setRegistry = (registry: Registry) => {
    this.registry = registry;
  }

  subscribeToChange = (listener: SubscriptionListener, shouldUpdate: ShouldUpdateSubscription = () => true): Unsubscribe => {
    const subscription = { listener, shouldUpdate };
    this.subscriptions.push(subscription);

    return () => {
      const index = this.subscriptions.indexOf(subscription);
      this.subscriptions.splice(index, 1);
    }
  }

  pushChanges = () => {
    for (const subscription of this.subscriptions) {
      if (subscription.shouldUpdate(this.dirtyViews)) {
        subscription.listener(this);
      }
    }
  }

  getItemType = (): string | null => {
    if (this.currentOperation) {
      return this.currentOperation.state.itemType;
    }
    return null;
  }

  getDragSourceId = (): string | null => {
    if (this.currentOperation) {
      return this.currentOperation.state.dragSourceId;
    }
    return null;
  }

  getDragSourceViewModel = (): DragSourceViewModelImpl | null => {
    if (this.currentOperation) {
      return this.currentOperation.state.dragSourceViewModel;
    }
    return null;
  }

  getDropTargetIds = (): string[] | null => {
    if (this.currentOperation) {
      return this.currentOperation.state.dropTargetIds;
    }
    return null;
  }

  isDragging = (): boolean => {
    if (this.currentOperation) {
      return true;
    }
    return false;
  }

  canDragSource = (id: string): boolean => {
    const viewModel = this.registry.getDragSourceViewModel(id);
    if (viewModel) { return viewModel.canDrag() }
    return false;
  }

  isDraggingSource = (id: string): boolean => {
    if (!this.currentOperation) {
      return false;
    }
    return this.currentOperation.state.dragSourceId === id;
  }

  isOverTarget = (id: string): boolean => {
    if (!this.currentOperation) {
      return false
    }
    return this.currentOperation.state.dropTargetIds.indexOf(id) >= 0;
  }

  getClientOffset = (): XYCoordinate | null => {
    if (this.currentOperation) {
      return this.currentOperation.offsets.clientOffset;
    }
    return null;
  }

  getSourceClientOffset = (): XYCoordinate | null => {
    if (this.currentOperation) {
      return this.currentOperation.offsets.sourceClientOffset;
    }
    return null;
  }


  getInitialClientOffset = (): XYCoordinate | null => {
    if (this.currentOperation) {
      return this.currentOperation.offsets.initialClientOffset;
    }
    return null;
  }

  getInitialSourceClientOffset = (): XYCoordinate | null => {
    if (this.currentOperation) {
      return this.currentOperation.offsets.initialSourceClientOffset;
    }
    return null
  }

  getSourceClientOffsetDifference = (): XYCoordinate | null => {
    if (this.currentOperation) {
      return this.currentOperation.offsets.diff;
    }
    return null;
  }

  getDropResult = (): DropResult | null => {
    if (this.currentOperation) {
      return this.currentOperation.result;
    }
    return null;
  }

  didDrop = (): boolean => {
    if (this.getDropResult()) {
      return true;
    }
    return false;
  }

  beginDrag = (id: string, event: MouseEvent) => {
    const dragSourceViewModel = this.registry.getDragSourceViewModel(id);
    const sourcePosition = (event.target as HTMLElement).getBoundingClientRect();

    this.currentOperation = {
      state: {
        dragSourceId: id,
        dropTargetIds: [],
        itemType: dragSourceViewModel.itemType,
        dragSourceViewModel,
      },
      offsets: {
        clientOffset: { x: event.clientX, y: event.clientY },
        sourceClientOffset: { x: sourcePosition.left, y: sourcePosition.top },
        initialClientOffset: { x: event.clientX, y: event.clientY },
        initialSourceClientOffset: { x: sourcePosition.left, y: sourcePosition.top },
        diff: { x: event.clientX - sourcePosition.left, y: event.clientY - sourcePosition.top }
      },
      result: null,
    }

    this.dirtyViews = { dragSources: [id], dropTargets: [] };

    this.pushChanges();

    this.dirtyViews = null;

    dragSourceViewModel.beginDrag();
  }

  move = (event: MouseEvent) => {
    const diff = this.currentOperation.offsets.diff;
    const sourcePosition = { x: event.clientX - diff.x, y: event.clientY - diff.y };
    const clientOffset = { x: event.clientX, y: event.clientY };
    const sourceClientOffset = sourcePosition;

    this.currentOperation.offsets.clientOffset = clientOffset;
    this.currentOperation.offsets.sourceClientOffset = sourceClientOffset;

    const newDropTargetIds = [];
    const previousDropTargetIds = [...this.getDropTargetIds()];

    for (const dropTargetId in this.registry.dropTargets) {
      const viewModel = this.registry.getDropTargetViewModel(dropTargetId);

      if (this.getItemType() !== viewModel.itemType) {
        continue;
      }

      const { top, bottom, left, right } = viewModel.element.getBoundingClientRect();
      if (
        clientOffset.x >= left &&
        clientOffset.x <= right &&
        clientOffset.y >= top &&
        clientOffset.y <= bottom
      ) {
        newDropTargetIds.push(dropTargetId);
      }
    }

    const dragSourceViewModel = this.currentOperation.state.dragSourceViewModel;

    if (dragSourceViewModel.autoScroll) {
      const element = dragSourceViewModel.autoScroll.element
      const { top, bottom, left, right } = element.getBoundingClientRect();
      const direction = dragSourceViewModel.autoScroll.direction || 'both';
      const offset = dragSourceViewModel.autoScroll.offset || 20;
      let scrollLeft = 0;
      let scrollTop = 0;

      if (direction !== 'vertical') {
        if (Math.abs(clientOffset.x - left) <= offset) {
          scrollLeft = -5;
        }
        if (Math.abs(clientOffset.x - right) <= offset) {
          scrollLeft = 5;
        }
      }

      if (direction !== 'horizontal') {
        if (Math.abs(clientOffset.y - top) <= offset) {
          scrollTop = -5;
        }
        if (Math.abs(clientOffset.y - bottom) <= offset) {
          scrollTop = 5;
        }
      }

      element.scrollBy({ left: scrollLeft, top: scrollTop, behavior: 'smooth' });
    }

    this.currentOperation.state.dropTargetIds = newDropTargetIds;

    this.dirtyViews = { dragSources: [this.getDragSourceId()], dropTargets: [...newDropTargetIds, ...previousDropTargetIds] };

    this.pushChanges();

    this.dirtyViews = null;

    for (const dropTargetId of this.getDropTargetIds()) {
      const viewModel = this.registry.getDropTargetViewModel(dropTargetId);
      viewModel.hover();
    }
  }

  drop = (event: Event) => {
    for (const dropTargetId of this.getDropTargetIds()) {
      const viewModel = this.registry.getDropTargetViewModel(dropTargetId);

      if (viewModel.canDrop()) {
        const result = viewModel.drop();

        if (result) {
          this.currentOperation.result = result;
        }
      }
    }

    this.dirtyViews = { dragSources: [this.getDragSourceId()], dropTargets: [...this.getDropTargetIds()] };

    this.pushChanges();

    this.getDragSourceViewModel().endDrag();
  }

  endDrag = () => {
    this.dirtyViews = { dragSources: [this.getDragSourceId()], dropTargets: [...this.getDropTargetIds()] };
    this.currentOperation = null;

    this.pushChanges();

    this.dirtyViews = null;
  }
}
