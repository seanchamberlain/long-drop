import Model, { ShouldUpdateSubscription } from "./Model";
import Controller from "./Controller";
import { DragSourceViewModelImpl } from "./DragSourceViewModel";
import { DropTargetViewModelImpl } from "./DropTargetViewModel";

export type Unsubscribe = () => void;

export default class Registry {
  controller: Controller;
  model: Model;
  dragSources: { [key: string]: { viewModel: DragSourceViewModelImpl, subscriptions: Unsubscribe[] } } = {};
  dropTargets: { [key: string]: { viewModel: DropTargetViewModelImpl, subscriptions: Unsubscribe[] } } = {};

  constructor(model: Model, controller: Controller) {
    this.model = model;
    this.controller = controller;
  }

  getDragSourceViewModel = (id: string): DragSourceViewModelImpl | null => {
    return (this.dragSources[id] && this.dragSources[id].viewModel) || null;
  }
  getDropTargetViewModel = (id: string): DropTargetViewModelImpl | null => {
    return (this.dropTargets[id] && this.dropTargets[id].viewModel) || null;
  }

  registerDragSourceViewModel = (viewModel: DragSourceViewModelImpl): void => {
    const shouldUpdate: ShouldUpdateSubscription = (dirtyView) => {
      return dirtyView.dragSources.indexOf(viewModel.id) >= 0;
    }

    const unsubscribeModel = this.model.subscribeToChange(viewModel.handleChange, shouldUpdate);
    const unsubscribeController = this.controller.registerDragSource(viewModel.id, viewModel.element);

    const subscriptions = [unsubscribeController, unsubscribeModel];

    this.dragSources[viewModel.id] = { viewModel, subscriptions };
  }
  registerDropTargetViewModel = (viewModel: DropTargetViewModelImpl): void => {
    const shouldUpdate: ShouldUpdateSubscription = (dirtyView) => {
      return dirtyView.dropTargets.indexOf(viewModel.id) >= 0;
    }

    const unsubscribeModel = this.model.subscribeToChange(viewModel.handleChange, shouldUpdate);

    const subscriptions = [unsubscribeModel];

    this.dropTargets[viewModel.id] = { viewModel, subscriptions };
  }

  unregisterDragSourceViewModel = (viewModel: DragSourceViewModelImpl) => {
    if (this.dragSources[viewModel.id]) {
      this.dragSources[viewModel.id].subscriptions.forEach(unsubscribe => unsubscribe());
      delete this.dragSources[viewModel.id];
    }
  }
  unregisterDropTargetViewModel = (viewModel: DropTargetViewModelImpl) => {
    if (this.dropTargets[viewModel.id]) {
      this.dropTargets[viewModel.id].subscriptions.forEach(unsubscribe => unsubscribe());
      delete this.dropTargets[viewModel.id];
    }
  }
}
