import * as React from 'react';
import { mount } from 'enzyme';

import DragSourceViewModel, { RegisterRef, DragSourceViewModelImpl } from '../DragSourceViewModel';
import DragDropProvider from '../Provider';

const mockUnregister = jest.fn();
const mockRegister = jest.fn();
const mockCanDrag = jest.fn();
const mockBeginDrag = jest.fn();
const mockEndDrag = jest.fn();

jest.mock('../Registry', () => jest.fn().mockImplementation(() => ({
  unregisterDragSourceViewModel: mockUnregister,
  registerDragSourceViewModel: mockRegister,
})));

class Component extends React.Component<{ registerRef: RegisterRef}> {
  render() {
    const { registerRef } = this.props;

    return <div ref={registerRef} />
  }
}

const DragSourceViewModelComponent = DragSourceViewModel(
  (props) => props.id || 'id',
  'itemType',
  {
    canDrag: mockCanDrag,
    endDrag: mockEndDrag,
    beginDrag: mockBeginDrag,
  },
  (id, model, registerRef) => ({ registerRef })
)(Component)

beforeEach(() => {
  jest.clearAllMocks();
});

test('it registers when mounting', () => {
  mount(
    <DragDropProvider>
      <DragSourceViewModelComponent />
    </DragDropProvider>
  )

  expect(mockRegister).toHaveBeenCalled();
});

test('it unregisters when unmounting', () => {
  const wrapper = mount(
    <DragDropProvider>
      <DragSourceViewModelComponent />
    </DragDropProvider>
  )

  mockRegister.mockClear();

  wrapper.unmount();

  expect(mockUnregister).toHaveBeenCalled();
});

describe('it calls spec methods', () => {
  test('with full spec', () => {
    const viewModel = new DragSourceViewModelComponent({});
    viewModel.canDrag();
    viewModel.beginDrag();
    viewModel.endDrag();

    expect(mockCanDrag).toHaveBeenCalled();
    expect(mockBeginDrag).toHaveBeenCalled();
    expect(mockEndDrag).toHaveBeenCalled();
  });


})

test('it calls spec methods', () => {
  
});

test('it re-registers if id changes', () => {
  const Wrapper = ({ id }) => (
    <DragDropProvider>
      <DragSourceViewModelComponent id={id} />
    </DragDropProvider>
  )

  const wrapper = mount(<Wrapper id="1" />);

  const dragSourceViewModelWrapper = wrapper.find(DragSourceViewModelComponent);
  const dragSourceViewModel = dragSourceViewModelWrapper.instance() as DragSourceViewModelImpl;

  expect(dragSourceViewModel.id).toBe('1');

  mockRegister.mockClear();
  mockUnregister.mockClear();

  wrapper.setProps({ id: '2' });

  expect(dragSourceViewModel.id).toBe('2');
  expect(mockUnregister).toHaveBeenCalled();
  expect(mockRegister).toHaveBeenCalled();
});

