import Controller from '../Controller';
import Model from '../Model';

const element = document.createElement('div');

const mockMove = jest.fn();
const mockCanDragSource = jest.fn();
const mockBeginDrag = jest.fn();
const mockDrop = jest.fn();
const mockEndDrag = jest.fn();

jest.mock('../Model', () => jest.fn().mockImplementation(() => ({
  canDragSource: mockCanDragSource,
  move: mockMove,
  beginDrag: mockBeginDrag,
  drop: mockDrop,
  endDrag: mockEndDrag,
})));

let model: Model;
let controller: Controller;

beforeEach(() => {
  model = new Model();
  controller = new Controller(model);
});

describe('it handles registration', () => {
  test('for DragSourceViewModels', () => {
    const addListenerSpy = jest.spyOn(element, 'addEventListener');
    controller.registerDragSource('DragSource(1)', element);

    expect(addListenerSpy).toHaveBeenCalledWith('mousedown', expect.any(Function));
  });
});

describe('it handles unsubscribing', () => {
  test('for DragSourceViewModels', () => {
    const removeListenerSpy = jest.spyOn(element, 'removeEventListener');
    const unsubscribe = controller.registerDragSource('DragSource(1)', element);

    unsubscribe();

    expect(removeListenerSpy).toHaveBeenCalledWith('mousedown', expect.any(Function));
  });
});

describe('it handles mouse events', () => {
  beforeEach(() => {
    controller.registerDragSource('DragSource(1)', element);
  });


  describe('mousedown', () => {
    test('doesnt fire beginDrag when canDragSource is false', () => {
      mockCanDragSource.mockReturnValueOnce(false);

      const event = new MouseEvent('mousedown');
      element.dispatchEvent(event);

      expect(mockBeginDrag).not.toHaveBeenCalled();
    });

    test('does fire beginDrag and adds listeners when canDragSource is true', () => {
      const addListenerSpy = jest.spyOn(document, 'addEventListener');

      mockCanDragSource.mockRejectedValueOnce(true);

      const event = new MouseEvent('mousedown');
      element.dispatchEvent(event);

      expect(mockBeginDrag).toHaveBeenCalled();
      expect(addListenerSpy).toHaveBeenCalledTimes(2);
      expect(addListenerSpy.mock.calls[0][0]).toBe('mousemove');
      expect(addListenerSpy.mock.calls[1][0]).toBe('mouseup');
    });
  });

  describe('mousemove', () => {
    beforeEach(() => {
      mockCanDragSource.mockReturnValueOnce(true);

      const event = new MouseEvent('mousedown');
      element.dispatchEvent(event);
    });

    test('it fires move', () => {
      const event = new MouseEvent('mousemove');
      document.dispatchEvent(event);

      expect(mockMove).toHaveBeenCalled();
    });
  });

  describe('mouseup', () => {
    beforeEach(() => {
      mockCanDragSource.mockReturnValueOnce(true);

      const event = new MouseEvent('mousedown');
      element.dispatchEvent(event);
    });

    test('it fires endDrag', () => {
      const removeListenerSpy = jest.spyOn(document, 'removeEventListener');

      const event = new MouseEvent('mouseup');
      document.dispatchEvent(event);

      expect(mockDrop).toHaveBeenCalled();
      expect(mockEndDrag).toHaveBeenCalled();
      expect(removeListenerSpy).toHaveBeenCalledTimes(2);
      expect(removeListenerSpy.mock.calls[0][0]).toBe('mousemove');
      expect(removeListenerSpy.mock.calls[1][0]).toBe('mouseup');
    });
  });
});
