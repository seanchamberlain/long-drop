import * as React from 'react';
import { shallow } from 'enzyme';

import Model from '../Model';
import Controller from '../Controller';
import Registry from '../Registry';
import DragDropProvider from '../Provider';

jest.mock('../Model', () => jest.fn().mockImplementation(() => ({
  setRegistry: () => null,
})));
jest.mock('../Controller');
jest.mock('../Registry');

test('it should set state correctly', () => {
  const wrapper = shallow(<DragDropProvider />);

  expect(Model).toHaveBeenCalled();
  expect(Controller).toHaveBeenCalled();
  expect(Registry).toHaveBeenCalled();

  expect(wrapper.state('registry')).toBe(((Registry as any)as jest.Mock).mock.instances[0]);
  expect(wrapper.state('controller')).toBe(((Controller as any)as jest.Mock).mock.instances[0]);
  expect(wrapper.state('model')).toBe(((Model as any)as jest.Mock).mock.results[0].value);
});
