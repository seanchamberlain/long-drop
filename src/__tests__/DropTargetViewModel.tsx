import * as React from 'react';
import { mount } from 'enzyme';

import DropTargetViewModel, { RegisterRef, DropTargetViewModelImpl } from '../DropTargetViewModel';
import DragDropProvider from '../Provider';

const mockUnregister = jest.fn();
const mockRegister = jest.fn();
const mockCanDrop = jest.fn();
const mockHover = jest.fn();
const mockDrop = jest.fn();

jest.mock('../Registry', () => jest.fn().mockImplementation(() => ({
  unregisterDropTargetViewModel: mockUnregister,
  registerDropTargetViewModel: mockRegister,
})));

class Component extends React.Component<{ registerRef: RegisterRef}> {
  render() {
    const { registerRef } = this.props;

    return <div ref={registerRef} />
  }
}

const DropTargetViewModelComponent = DropTargetViewModel(
  (props) => props.id || 'id',
  'itemType',
  {
    canDrop: mockCanDrop,
    hover: mockHover,
    drop: mockDrop,
  },
  (id, model, registerRef) => ({ registerRef })
)(Component)

beforeEach(() => {
  jest.clearAllMocks();
});

test('it registers when mounting', () => {
  mount(
    <DragDropProvider>
      <DropTargetViewModelComponent />
    </DragDropProvider>
  )

  expect(mockRegister).toHaveBeenCalled();
});

test('it unregisters when unmounting', () => {
  const wrapper = mount(
    <DragDropProvider>
      <DropTargetViewModelComponent />
    </DragDropProvider>
  )

  mockRegister.mockClear();

  wrapper.unmount();

  expect(mockUnregister).toHaveBeenCalled();
});

describe('it calls spec methods', () => {
  test('with full spec', () => {
    const viewModel = new DropTargetViewModelComponent({});
    viewModel.canDrop();
    viewModel.hover();
    viewModel.drop();

    expect(mockCanDrop).toHaveBeenCalled();
    expect(mockHover).toHaveBeenCalled();
    expect(mockDrop).toHaveBeenCalled();
  });


})

test('it calls spec methods', () => {
  
});

test('it re-registers if id changes', () => {
  const Wrapper = ({ id }) => (
    <DragDropProvider>
      <DropTargetViewModelComponent id={id} />
    </DragDropProvider>
  )

  const wrapper = mount(<Wrapper id="1" />);

  const dropTargetViewModelWrapper = wrapper.find(DropTargetViewModelComponent);
  const dropTargetViewModel = dropTargetViewModelWrapper.instance() as DropTargetViewModelImpl;

  expect(dropTargetViewModel.id).toBe('1');

  mockRegister.mockClear();
  mockUnregister.mockClear();

  wrapper.setProps({ id: '2' });

  expect(dropTargetViewModel.id).toBe('2');
  expect(mockUnregister).toHaveBeenCalled();
  expect(mockRegister).toHaveBeenCalled();
});

