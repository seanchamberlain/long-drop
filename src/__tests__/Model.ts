import * as React from 'react';

import Registry from '../Registry';
import Model from '../Model';
import DragSourceViewModel, { DragSourceViewModelImpl } from '../DragSourceViewModel';
import DropTargetViewModel, { DropTargetViewModelImpl } from '../DropTargetViewModel';
import Controller from '../Controller';

const mockBeginDrag = jest.fn();
const mockGetDragSourceViewModel = jest.fn();
const mockGetDropTargetViewModel = jest.fn();
const mockSubscription = jest.fn();
const mockShouldUpdate = jest.fn();
const mockCanDrag = jest.fn();
const mockHover = jest.fn();
const mockDrop = jest.fn();
const mockCanDrop = jest.fn();
const mockEndDrag = jest.fn();

mockShouldUpdate.mockReturnValue(true);
mockCanDrag.mockReturnValue(true);
mockCanDrop.mockReturnValue(true);
mockDrop.mockReturnValue({ });

const element: HTMLDivElement = document.createElement('div');;
const dropElement: HTMLDivElement = document.createElement('div');

jest.mock('../Registry', () => jest.fn().mockImplementation(() => ({
  getDragSourceViewModel: mockGetDragSourceViewModel,
  getDropTargetViewModel: mockGetDropTargetViewModel,
})));
jest.mock('../Controller', () => jest.fn().mockImplementation(() => ({
})));
jest.mock('../DragSourceViewModel', () => jest.fn().mockImplementation((id: () => string, itemType: string) =>
  () => jest.fn().mockImplementation(() => ({
    id: id(),
    itemType,
    beginDrag: mockBeginDrag,
    canDrag: mockCanDrag,
    element,
    endDrag: mockEndDrag,
  })),
));
jest.mock('../DropTargetViewModel', () => jest.fn().mockImplementation((id: () => string, itemType: string) =>
  () => jest.fn().mockImplementation(() => ({
    id: id(),
    itemType,
    element: dropElement,
    hover: mockHover,
    drop: mockDrop,
    canDrop: mockCanDrop,
  })),
));

class MockComponent extends React.Component {};

let registry: Registry;
let model: Model;
let controller: Controller;
let dragSourceViewModel: DragSourceViewModelImpl;
let dropTargetViewModel: DropTargetViewModelImpl;

beforeEach(() => {
  jest.clearAllMocks();

  model = new Model();
  controller = new Controller(model);
  registry = new Registry(model, controller);
  model.setRegistry(registry);
});

describe('subscribeToChange', () => {
  test('the return function unsubscribes', () => {
    const unsubscribe = model.subscribeToChange(() => null, () => true);
    expect(model.subscriptions).toHaveLength(1);

    unsubscribe();
    expect(model.subscriptions).toHaveLength(0);
  });

  test('with default shouldUpdate', () => {
    const unsubscribe = model.subscribeToChange(() => null);
    expect(model.subscriptions[0].shouldUpdate({ dragSources: [], dropTargets: []})).toBeTruthy();
  });
});

describe('lifecycle', () => {
  beforeEach(() => {
    dragSourceViewModel = new (DragSourceViewModel(() => 'DragSource(1)', 'itemType', {}, () => ({}))(MockComponent))({});
    dropTargetViewModel = new (DropTargetViewModel(() => 'DropTarget(1)', 'itemType', {}, () => ({}))(MockComponent))({});

    registry.dropTargets = { 'DropTarget(1)': { viewModel: dropTargetViewModel, subscriptions: [] } };
    registry.dragSources = { 'DragSource(1)': { viewModel: dragSourceViewModel, subscriptions: [] } };

    mockGetDragSourceViewModel.mockReturnValue(dragSourceViewModel);
    mockGetDropTargetViewModel.mockReturnValue(dropTargetViewModel);

    const subscription = { listener: mockSubscription, shouldUpdate: mockShouldUpdate }
    model.subscriptions = [subscription];

    document.body.appendChild(element);
    element.getBoundingClientRect = () => ({ x: 0, y: 0, top: 0, left: 0, width: 100, height: 100, bottom: 100, right: 100 });

    document.body.appendChild(dropElement);
    dropElement.getBoundingClientRect = () => ({ x: 100, y: 100, top: 100, left: 100, width: 100, height: 100, bottom: 200, right: 200 });
  });

  describe('canDrag', () => {
    test('returns false', () => {
      mockCanDrag.mockReturnValueOnce(false);

      expect(model.canDragSource('DragSource(1)')).toBe(false);
    });

    test('returns true', () => {
      expect(model.canDragSource('DragSource(1)')).toBe(true);
    });

    test('viewModel is undefined', () => {
      mockGetDragSourceViewModel.mockReturnValueOnce(undefined);

      expect(model.canDragSource('DragSource(1)')).toBe(false);
    });
  });

  describe('when there is no currentOperation', () => {
    test('everything should be null', () => {
      expect(model.isDraggingSource('DragSource(1)')).toBeFalsy();
      expect(model.isDragging()).toBeFalsy();
      expect(model.getItemType()).toBeNull();
      expect(model.getDragSourceId()).toBeNull();
      expect(model.getDragSourceViewModel()).toBeNull();
      expect(model.getDropTargetIds()).toBeNull();
      expect(model.isOverTarget('DropTarget(1)')).toBeFalsy();
      expect(model.getDropResult()).toBeNull();
      expect(model.didDrop()).toBeFalsy();
      expect(model.getInitialClientOffset()).toBeNull();
      expect(model.getInitialSourceClientOffset()).toBeNull();
      expect(model.getClientOffset()).toBeNull();
      expect(model.getSourceClientOffset()).toBeNull();
      expect(model.getSourceClientOffsetDifference()).toBeNull();
    });
  });

  describe('after a beginDrag operation', () => {
    beforeEach(() => {
      const event = new MouseEvent('mousedown', { clientX: 50, clientY: 50 });
      element.dispatchEvent(event);

      model.beginDrag('DragSource(1)', event);
    });

    test('everything should return correct value', () => {
      expect(model.isDraggingSource('DragSource(1)')).toBeTruthy();
      expect(model.isDragging()).toBeTruthy();
      expect(model.getItemType()).toBe('itemType');
      expect(model.getDragSourceId()).toBe('DragSource(1)');
      expect(model.getDragSourceViewModel()).toBe(dragSourceViewModel);
      expect(model.getDropTargetIds()).toHaveLength(0);
      expect(model.isOverTarget('DropTarget(1)')).toBeFalsy();
      expect(model.getDropResult()).toBeNull();
      expect(model.didDrop()).toBeFalsy();
      expect(model.getInitialClientOffset()).toMatchObject({ x: 50, y: 50 });
      expect(model.getInitialSourceClientOffset()).toMatchObject({ x: 0, y: 0 });
      expect(model.getClientOffset()).toMatchObject({ x: 50, y: 50 });
      expect(model.getSourceClientOffset()).toMatchObject({ x: 0, y: 0 });
      expect(model.getSourceClientOffsetDifference()).toMatchObject({ x: 50, y: 50 });
    });

    test('it should call the dragSourceViewModel beginDrag method', () => {
      expect(mockBeginDrag).toHaveBeenCalled();
    });

    test('it should call subscriptions', () => {
      expect(mockShouldUpdate).toHaveBeenCalled();
      expect(mockSubscription).toHaveBeenCalled();
    });
  });


  describe('after a move operation', () => {
    beforeEach(() => {
      const startEvent = new MouseEvent('mousedown', { clientX: 50, clientY: 50 });
      element.dispatchEvent(startEvent);

      model.beginDrag('DragSource(1)', startEvent);
    });

    describe('with same itemType', () => {
      beforeEach(() => {
        const moveEvent = new MouseEvent('mousemove', { clientX: 150, clientY: 150 });
        document.dispatchEvent(moveEvent);

        model.move(moveEvent);
      });

      test('everything should return correct value', () => {
        expect(model.isDraggingSource('DragSource(1)')).toBeTruthy();
        expect(model.isDragging()).toBeTruthy();
        expect(model.getItemType()).toBe('itemType');
        expect(model.getDragSourceId()).toBe('DragSource(1)');
        expect(model.getDragSourceViewModel()).toBe(dragSourceViewModel);
        expect(model.getDropTargetIds()).toHaveLength(1);
        expect(model.getDropTargetIds()[0]).toBe('DropTarget(1)');
        expect(model.isOverTarget('DropTarget(1)')).toBeTruthy();
        expect(model.getDropResult()).toBeNull();
        expect(model.didDrop()).toBeFalsy();
        expect(model.getInitialClientOffset()).toMatchObject({ x: 50, y: 50 });
        expect(model.getInitialSourceClientOffset()).toMatchObject({ x: 0, y: 0 });
        expect(model.getClientOffset()).toMatchObject({ x: 150, y: 150 });
        expect(model.getSourceClientOffset()).toMatchObject({ x: 100, y: 100 });
        expect(model.getSourceClientOffsetDifference()).toMatchObject({ x: 50, y: 50 });
      });

      test('it should call the dropTargetViewModel hover method', () => {
        expect(mockHover).toHaveBeenCalled();
      });

      test('it should call subscriptions', () => {
        expect(mockShouldUpdate).toHaveBeenCalledTimes(2);
        expect(mockSubscription).toHaveBeenCalledTimes(2);
      });
    });


    describe('with different item type', () => {
      beforeEach(() => {
        const differentDropTargetViewModel = new (DropTargetViewModel(() => 'DropTarget(2)', 'itemType1', {}, () => ({}))(MockComponent))({});

        registry.dropTargets = { 'DropTarget(2)': { viewModel: differentDropTargetViewModel, subscriptions: [] } };
        registry.dragSources = { 'DragSource(1)': { viewModel: dragSourceViewModel, subscriptions: [] } };

        mockGetDropTargetViewModel.mockReturnValueOnce(differentDropTargetViewModel);

        const moveEvent = new MouseEvent('mousemove', { clientX: 150, clientY: 150 });
        document.dispatchEvent(moveEvent);

        model.move(moveEvent);
      });

      test('it shouldnt update state', () => {
        expect(model.getDropTargetIds()).toHaveLength(0);
        expect(model.isOverTarget('DropTarget(2)')).toBeFalsy();
        expect(mockHover).not.toHaveBeenCalled();
      });
    });
  });


  describe('after a drop operation', () => {
    beforeEach(() => {
      const startEvent = new MouseEvent('mousedown', { clientX: 50, clientY: 50 });
      element.dispatchEvent(startEvent);

      model.beginDrag('DragSource(1)', startEvent);

      const moveEvent = new MouseEvent('mousemove', { clientX: 150, clientY: 150 });
      document.dispatchEvent(moveEvent);

      model.move(moveEvent);

      const dropEvent = new MouseEvent('mouseup', { clientX: 150, clientY: 150 });
      document.dispatchEvent(dropEvent);
      model.drop(dropEvent);
    });

    test('everything should return correct value', () => {
      expect(model.isDraggingSource('DragSource(1)')).toBeTruthy();
      expect(model.isDragging()).toBeTruthy();
      expect(model.getItemType()).toBe('itemType');
      expect(model.getDragSourceId()).toBe('DragSource(1)');
      expect(model.getDragSourceViewModel()).toBe(dragSourceViewModel);
      expect(model.getDropTargetIds()).toHaveLength(1);
      expect(model.getDropTargetIds()[0]).toBe('DropTarget(1)');
      expect(model.isOverTarget('DropTarget(1)')).toBeTruthy();
      expect(model.getDropResult()).toMatchObject({});
      expect(model.didDrop()).toBeTruthy();
      expect(model.getInitialClientOffset()).toMatchObject({ x: 50, y: 50 });
      expect(model.getInitialSourceClientOffset()).toMatchObject({ x: 0, y: 0 });
      expect(model.getClientOffset()).toMatchObject({ x: 150, y: 150 });
      expect(model.getSourceClientOffset()).toMatchObject({ x: 100, y: 100 });
      expect(model.getSourceClientOffsetDifference()).toMatchObject({ x: 50, y: 50 });
    });

    test('it should call the dragSourceViewModel endDrag method', () => {
      expect(mockEndDrag).toHaveBeenCalled();
    });

    test('it should call subscriptions', () => {
      expect(mockShouldUpdate).toHaveBeenCalledTimes(3);
      expect(mockSubscription).toHaveBeenCalledTimes(3);
    });
  });

  describe('after an endDrag operation', () => {
    beforeEach(() => {
      const startEvent = new MouseEvent('mousedown', { clientX: 50, clientY: 50 });
      element.dispatchEvent(startEvent);

      model.beginDrag('DragSource(1)', startEvent);

      const moveEvent = new MouseEvent('mousemove', { clientX: 150, clientY: 150 });
      document.dispatchEvent(moveEvent);

      model.move(moveEvent);

      const dropEvent = new MouseEvent('mouseup', { clientX: 150, clientY: 150 });
      document.dispatchEvent(dropEvent);
      model.drop(dropEvent);
      model.endDrag();
    });

    test('everything should return correct value', () => {
      expect(model.isDraggingSource('DragSource(1)')).toBeFalsy();
      expect(model.isDragging()).toBeFalsy();
      expect(model.getItemType()).toBeNull();
      expect(model.getDragSourceId()).toBeNull();
      expect(model.getDragSourceViewModel()).toBeNull();
      expect(model.getDropTargetIds()).toBeNull();
      expect(model.isOverTarget('DropTarget(1)')).toBeFalsy();
      expect(model.getDropResult()).toBeNull();
      expect(model.didDrop()).toBeFalsy();
      expect(model.getInitialClientOffset()).toBeNull();
      expect(model.getInitialSourceClientOffset()).toBeNull();
      expect(model.getClientOffset()).toBeNull();
      expect(model.getSourceClientOffset()).toBeNull();
      expect(model.getSourceClientOffsetDifference()).toBeNull();
    });

    test('it should call subscriptions', () => {
      expect(mockShouldUpdate).toHaveBeenCalledTimes(4);
      expect(mockSubscription).toHaveBeenCalledTimes(4);
    });
  });
});
