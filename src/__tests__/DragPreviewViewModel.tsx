import * as React from 'react';
import { mount } from 'enzyme';

import DragPreviewViewModel from '../DragPreviewViewModel';
import DragDropProvider from '../Provider';

const mockUnsubscribe = jest.fn();
const mockSubscribe = jest.fn(() => mockUnsubscribe);

jest.mock('../Model', () => jest.fn().mockImplementation(() => ({
  subscribeToChange: mockSubscribe,
  setRegistry: () => null,
})));

class Component extends React.Component {
  render() {
    return <div />
  }
}

const DragPreviewViewModelComponent = DragPreviewViewModel((model) => ({ }))(Component)

beforeEach(() => {
  jest.clearAllMocks();
});

test('it subscribes when mounting', () => {
  mount(
    <DragDropProvider>
      <DragPreviewViewModelComponent />
    </DragDropProvider>
  )

  expect(mockSubscribe).toHaveBeenCalled();
});

test('it unregisters when unmounting', () => {
  const wrapper = mount(
    <DragDropProvider>
      <DragPreviewViewModelComponent />
    </DragDropProvider>
  )

  wrapper.unmount();

  expect(mockUnsubscribe).toHaveBeenCalled();
});

