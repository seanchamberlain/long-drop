import * as React from 'react';

import Registry from '../Registry';
import Controller from '../Controller';
import Model, { DirtyViews } from '../Model';
import DragSourceViewModel from '../DragSourceViewModel';
import DropTargetViewModel from '../DropTargetViewModel';

let shouldUpdate: (dirtyViews: DirtyViews) => boolean;

const mockSubscribeToChange = jest.fn().mockImplementation((listener, shouldUpdateFn) => {
  shouldUpdate = shouldUpdateFn;
});
const mockRegisterDragSource = jest.fn();
const mockUnsubscribe = jest.fn();

jest.mock('../Controller', () => jest.fn().mockImplementation(() => ({
  registerDragSource: mockRegisterDragSource,
})));
jest.mock('../Model', () => jest.fn().mockImplementation(() => ({
  subscribeToChange: mockSubscribeToChange,
})));
jest.mock('../DragSourceViewModel', () => jest.fn().mockImplementation((id: () => string) =>
  () => jest.fn().mockImplementation(() => ({
    id: id(),
  })),
));
jest.mock('../DropTargetViewModel', () => jest.fn().mockImplementation((id: () => string) =>
  () => jest.fn().mockImplementation(() => ({
    id: id(),
  })),
));

let registry: Registry;
let model: Model;
let controller: Controller;

class MockComponent extends React.Component {};
const dragSourceViewModel = new (DragSourceViewModel(() => 'DragSource(1)', 'itemType', {}, () => ({}))(MockComponent))({});
const dropTargetViewModel = new (DropTargetViewModel(() => 'DropTarget(1)', 'itemType', {}, () => ({}))(MockComponent))({});

beforeEach(() => {
  jest.clearAllMocks();

  model = new Model();
  controller = new Controller(model);
  registry = new Registry(model, controller);
});

describe('it handles registration', () => {
  test('for DragSourceViewModels', () => {
    registry.registerDragSourceViewModel(dragSourceViewModel);

    expect(mockSubscribeToChange.mock.calls).toHaveLength(1);
    expect(mockRegisterDragSource.mock.calls).toHaveLength(1);
    expect(registry.dragSources).toHaveProperty('DragSource(1)');
    expect(shouldUpdate({ dragSources: ['DragSource(1)'], dropTargets: [] })).toBeTruthy();
    expect(shouldUpdate({ dragSources: ['DragSource(2)'], dropTargets: [] })).toBeFalsy();
  });

  test('for DropTargetViewModels', () => {
    registry.registerDropTargetViewModel(dropTargetViewModel);

    expect(mockSubscribeToChange.mock.calls).toHaveLength(1);
    expect(registry.dropTargets).toHaveProperty('DropTarget(1)');
    expect(shouldUpdate({ dragSources: [], dropTargets: ['DropTarget(1)'] })).toBeTruthy();
    expect(shouldUpdate({ dragSources: [], dropTargets: ['DropTarget(2)'] })).toBeFalsy();
  });
});

describe('it handles unregistering', () => {
  beforeEach(() => {
    registry.dragSources = { 'DragSource(1)': { viewModel: dragSourceViewModel, subscriptions: [mockUnsubscribe] } }
    registry.dropTargets = { 'DropTarget(1)': { viewModel: dropTargetViewModel, subscriptions: [mockUnsubscribe] } }
  });

  test('for DragSourceViewModels', () => {
    registry.unregisterDragSourceViewModel(dragSourceViewModel)

    expect(mockUnsubscribe.mock.calls).toHaveLength(1);
    expect(registry.dragSources).not.toHaveProperty('DragSource(1)')
  });

  test('for DropTargeteViewModels', () => {
    registry.unregisterDropTargetViewModel(dropTargetViewModel)

    expect(mockUnsubscribe.mock.calls).toHaveLength(1);
    expect(registry.dropTargets).not.toHaveProperty('DropTarget(1)')
  });
});

describe('it gets correctly', () => {
  beforeEach(() => {
    registry.dragSources = { 'DragSource(1)': { viewModel: dragSourceViewModel, subscriptions: [mockUnsubscribe] } }
    registry.dropTargets = { 'DropTarget(1)': { viewModel: dropTargetViewModel, subscriptions: [mockUnsubscribe] } }
  });

  test('for DragSourceViewModels', () => {
    expect(registry.getDragSourceViewModel('DragSource(1)')).toBe(dragSourceViewModel);
    expect(registry.getDragSourceViewModel('Invalid')).toBe(null);
  });

  test('for DropTargetViewModels', () => {
    expect(registry.getDropTargetViewModel('DropTarget(1)')).toBe(dropTargetViewModel);
    expect(registry.getDropTargetViewModel('Invalid')).toBe(null);
  });
});
