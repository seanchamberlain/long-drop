[![npm version](https://img.shields.io/npm/v/long-drop.svg)](https://www.npmjs.com/package/long-drop) ![Build Status](https://gitlab.com/seanchamberlain/long-drop/badges/master/build.svg?style=flat-square) ![gzip size](http://img.badgesize.io/https://npmcdn.com/long-drop/dist/index.js?compression=gzip)

# Long Drop
A simple React Drag and Drop Library that makes use of native mouse events.

Docs and examples can be found at:
http://example.com

### Installation:
`yarn add long-drop`
